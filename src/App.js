import React, { useState } from 'react';
import Header from './components/layout/header/Header'
import Menu from './components/layout/menu/Menu'
import Footer from './components/layout/footer/Footer'
import UserCreate from './components/user/Create'
import UserList from './components/user/List'
import Login from './components/login/Login'

const routes = {
    'List': {
        Pagina: UserList,
        showMenu: true,
        actions: {
            title: 'Usuários',
            button: 'Novo Usuário',
            to: 'Create'
        }
    },
    'Create': {
        Pagina: UserCreate,
        showMenu: true,
        actions: {
            title: 'Criar Usuário',
            button: 'Listar Usuários',
            to: 'List'
        }
    },
    'Login': {
        Pagina: Login,
        showMenu: false,
        actions: {
            title: 'Login',
            button: '',
            to: 'List'
        }
    }
}

function App() {

    const [page, setPage] = useState('Login')
    const {Pagina} = routes[page]

      return (
        <React.Fragment>
            <Header />
            <main>
                <Menu pagina={routes[page]} mudaPagina={setPage}/>
                <Pagina mudaPagina={setPage} />
            </main>
            <Footer />
        </React.Fragment>
    );
}

export default App;
