import React from 'react';

function Header(props) {

    return (
        <React.Fragment>
            <header>
                <h1>Cadastra Game</h1>
                <nav>
                    <ul>
                        <li>Página inicial</li>
                        <li>Jogos</li>
                        <li>Usuários</li>
                        <li>Login</li>
                    </ul>
                </nav>
            </header>
        </React.Fragment>
    );
}

export default Header;
