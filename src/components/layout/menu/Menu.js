import React from 'react';

function Menu(props) {
    const { title, button, to } = props.pagina.actions

    return (
        <React.Fragment>
            <div>
                <div id="titleMain">
                    <h2>{title}</h2>
                </div>
                <div>
                    <button onClick={() => props.mudaPagina(to)}>{button}</button>
                </div>
            </div>
        </React.Fragment>
    );
}

export default Menu;
