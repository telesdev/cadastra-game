import React from 'react';

function Login(props) {
    return (
        <React.Fragment>
        <div id="form">
            <form id="formLogin">
                <div className="form-group">
                    <label htmlFor="email">E-mail:</label>
                    <input type="text" name="email" placeholder=" Insira um email cadastrado" />
                </div>
                <div className="form-group">
                    <label htmlFor="senha">Senha:</label>
                    <input type="password" name="senha" placeholder=" Insira sua senha" />
                </div>                
                <div id="divButton">
                    <button onClick={() => props.mudaPagina('List')} className="submit">Entrar</button>
                    <button id="esqueci">Esqueci minha senha</button>           
                </div>
            </form>
        </div>
        </React.Fragment>
    );
}

export default Login;
