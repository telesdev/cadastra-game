import React, { useState } from 'react';
import { createUser } from '../../services/user'

function UserCreate(props) {

    const [form, setForm] = useState({})

    function handleChange(event) {
        setForm({
            ...form,
            [event.target.name] : event.target.value
        })
    }

    async function formSubmit(event){
        try {
            await createUser(form)
            alert("Formulário enviado")
            props.mudaPagina('List')
        } catch (error) {
            console.log(error.msg)
        }
    }

    function isFormValid(){
        return form.email && form.senha && form.nome && form.cidade && form.bairro && form.discord && form.telefone
    }

    return (
        <React.Fragment>
            <div id="form">
                <form id="formUsuarioCreate">
                    <div className="form-group">
                        <label htmlFor="email">*E-mail:</label>
                        <input type="text" name="email" onChange={handleChange} value={form.email || ""} placeholder=" Insira um email para cadastro" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="senha">*Senha:</label>
                        <input type="password" name="senha" onChange={handleChange} value={form.senha || ""} placeholder=" Insira uma senha válida" />
                    </div>                
                    <div className="form-group">
                        <label htmlFor="nome">*Nome:</label>
                        <input type="text" name="nome" onChange={handleChange} value={form.nome || ""} placeholder=" Insira seu nome" />
                    </div>
                    <fieldset className="form-group">
                        <legend>*Selecione os FG's que você joga:</legend>
                        <div className="field-group">
                            <input className="field-input" type="checkbox" name="jogos" onChange={handleChange} value="Dragon Ball FighterZ" />
                            <label htmlFor="dbz"> Dragon Ball FighterZ</label>
                        </div>
                        <div className="field-group">
                            <input className="field-input" type="checkbox" name="jogos" onChange={handleChange} value="Street Fighter V" /> 
                            <label htmlFor="sfv"> Street Fighter V</label>
                        </div>
                        <div className="field-group">
                            <input className="field-input" type="checkbox" name="jogos" onChange={handleChange} value="Tekken 7" /> 
                            <label htmlFor="tk7"> Tekken 7</label>
                        </div>
                        <div className="field-group">
                            <label htmlFor="outrosJogos">Outros:</label>
                            <input className="field-input" type="text" name="jogos" onChange={handleChange} /> 
                        </div>
                    </fieldset>
                    <div className="form-group">
                        <label htmlFor="cidade">*Cidade:</label>
                        <input type="text" name="cidade" onChange={handleChange} value={form.cidade || ""} placeholder=" Insira sua cidade" /> 
                    </div>
                    <div className="form-group">
                        <label htmlFor="bairro">*Bairro:</label>
                        <input type="text" name="bairro" onChange={handleChange} value={form.bairro || ""} placeholder=" Insira seu bairro" /> 
                    </div>
                    <div className="form-group">
                        <label htmlFor="discord">Discord:</label>
                        <input type="text" name="discord" onChange={handleChange} value={form.discord || ""} placeholder=" Insira seu usuário do Discord" />
                    </div>   
                    <div className="form-group">
                        <label htmlFor="telefone">Telefone:</label>
                        <input type="number" name="telefone" onChange={handleChange} value={form.telefone || ""} placeholder=" Insira seu telefone" />
                    </div>
                    <div id="divButton">
                        <button disabled={!isFormValid()} type="button" onClick={formSubmit} className="submit">Enviar</button>
                    </div>
                </form>
            </div>
        </React.Fragment>
    );
}

export default UserCreate;
