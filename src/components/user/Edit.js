import React from 'react';

function UserEdit() {
    return (
        <React.Fragment>
            <div id="titleMain">
                <h2>Editar Usuário</h2>
            </div>
            <div id="form">
                <form id="formUsuarioEdit" method="POST" action="http://localhost:3001/user">
                    <div className="form-group">
                        <label htmlFor="email">E-mail:</label>
                        <input type="text" name="email" placeholder=" Insira um email para cadastro" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="senha">Senha:</label>
                        <input type="password" name="senha" placeholder=" Insira uma senha válida" />
                    </div>                
                    <div className="form-group">
                        <label htmlFor="nome">Nome:</label>
                        <input type="text" name="nome" placeholder=" Insira seu nome" />
                    </div>
                    <fieldset className="form-group">
                        <legend>Selecione os FG's que você joga:</legend>
                        <div className="field-group">
                            <input className="field-input" type="checkbox" name="jogos" value="Dragon Ball FighterZ" />
                            <label htmlFor="dbz"> Dragon Ball FighterZ</label>
                        </div>
                        <div className="field-group">
                            <input className="field-input" type="checkbox" name="jogos" value="Street Fighter V" /> 
                            <label htmlFor="sfv"> Street Fighter V</label>
                        </div>
                        <div className="field-group">
                            <input className="field-input" type="checkbox" name="jogos" value="Tekken 7" /> 
                            <label htmlFor="tk7"> Tekken 7</label>
                        </div>
                        <div className="field-group">
                            <label htmlFor="outrosJogos">Outros:</label>
                            <input className="field-input" type="text" name="jogos" /> 
                        </div>
                    </fieldset>
                    <div className="form-group">
                        <label htmlFor="cidade">Cidade:</label>
                        <input type="text" name="cidade" placeholder=" Insira sua cidade" /> 
                    </div>
                    <div className="form-group">
                        <label htmlFor="bairro">Bairro:</label>
                        <input type="text" name="bairro" placeholder=" Insira seu bairro" /> 
                    </div>
                    <div className="form-group">
                        <label htmlFor="discord">Discord:</label>
                        <input type="text" name="discord" placeholder=" Insira seu usuário do Discord" />
                    </div>   
                    <div className="form-group">
                        <label htmlFor="telefone">Telefone:</label>
                        <input type="number" name="telefone" placeholder=" Insira seu telefone" />
                    </div>
                    <div id="divButton">
                        <button type="submit" className="submit">Enviar</button>
                    </div>
                </form>
            </div>
        </React.Fragment>
    );
}

export default UserEdit;
