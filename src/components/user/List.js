import React, { useEffect, useState } from 'react'
import { listUser } from '../../services/user'


function UserList() {

    const [users, setUsers] = useState([])

    const getList = async () => {
        try {
            const list = await listUser()
            setUsers(list.data)
        } catch (error) {
            
        }
    }

    const editUser = (user) => console.log('editando', user)

    const deleteUser = (user) => console.log('deletando', user)

    const setUpUsers = () => users.map((usuario, index) => (
            <tr key={index}>
                <td>{usuario.nome}</td>
                <td>{usuario.email}</td>
                <td>{usuario.jogos}</td>
                <td>{usuario.cidade}</td>
                <td>{usuario.bairro}</td>
                <td>{usuario.discord}</td>
                <td>{usuario.telefone}</td>
                <td><span onClick={() => editUser(usuario)}>Editar</span> | <span onClick={() => deleteUser(usuario)}>Excluir</span></td>
            </tr>
        ))

    useEffect(() => {
        getList()
    },[])
    
    return (
        <React.Fragment>
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Email</th>
                            <th>Jogos</th>
                            <th>Cidade</th>
                            <th>Bairro</th>
                            <th>Discord</th>
                            <th>Telefone</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        {setUpUsers()}
                    </tbody>
                </table>
            </div>
        </React.Fragment>
    );
}

export default UserList;
