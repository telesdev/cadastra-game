import axios from 'axios'

const clientHttp = axios.create({
    baseURL: 'http://localhost:3001',
    headers: { 'Content-Type' : 'application/json' }
})

export { clientHttp }