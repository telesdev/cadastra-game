import { clientHttp } from '../config/Config'

const createUser = (data) => clientHttp.post('/user', data)

const listUser = () => clientHttp.get('/user')

export{
    createUser,
    listUser
}

